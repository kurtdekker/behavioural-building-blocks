# B³ (Behavioural Building Blocks)
A system consisting of very simple actions, senses and planners that can be used to create autonomous agents fast. The project is made to be designer friendly so as to allow for easy plug-and-play development. These behaviours are **domain agnostic** and because of this, can be used in a wide variety of games. But it **should be noted** that these behaviours are not necessarily very complex. They are made to give life to a game very fast without having to write a lot of the same basic boilerplate code to make agents move and interact with the world.

The system relies entirely on the built-in Unity Navigation Mesh and Nav Agents so you can start making these agents fast. This project is also a great opporturnity to learn about Unity's built-in agents and navigational mesh, should you be a beginner in those areas.

# Vocabulary
This project uses a set of keywords that should be known to better navigate the scripts:
* **Action**  - An action can be used to give an agent life immediately. Some actions can only be added once per agent, some can be stacked.
* **Sense**   - A sense is made to give an agent senses. Senses can be used in various scenarios to give agents a greater sense of believability.
* **Planner** - A planner enables more advanced behaviour, combining already known *Actions* and *Senses*.
* **Gizmo**   - The project comes with various gizmos that can be attached to agents to better debug their behaviour in the editor, using Unity's built-in Gizmo visualisations.
